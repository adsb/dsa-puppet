#
class webserver::defaultpage {
	file { [ '/srv/www', '/srv/www/default.debian.org', '/srv/www/default.debian.org/htdocs', '/srv/www/default.debian.org/htdocs-disabled' ]:
		ensure  => directory,
		mode    => '0755',
	}

	file { '/srv/www/default.debian.org/htdocs/index.html':
		content => template('webserver/default-index.html'),
	}

	file { '/srv/www/default.debian.org/htdocs-disabled/index.html':
		content => template('webserver/disabled-index.html'),
	}
}
