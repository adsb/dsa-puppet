# service instance for Debian's mirror-health checker
#
# for each defined service, a daemon will regularly check if the
# local version of <url> matches the majority version among the
# set of all hosts that define a <service_name>.
#
# the result of this check is then exposed via <health_url>
#
# @param url URL to check for freshness on each host (normally a Release file)
# @param health_url URL to the health marker
# @param this_host_service_name this host's service name for this service.
#                               Other nodes will connect to this hostname to
#                               fetch url for checking.
#                               If this is undef, other hosts will not
#                               check this host.
# @param check_service name of this service
# @param check_interval how often to check
# @param ensure  present or absent
define mirror_health::service (
  String $url,
  String $health_url,
  Optional[String] $this_host_service_name = undef,
  String $check_service = $name,
  Integer $check_interval = 60,
  Enum['present','absent'] $ensure = 'present',
) {
  include mirror_health

  $service_file = "/etc/systemd/system/mirror-health-${check_service}.service"

  file { $service_file:
    content => template('mirror_health/mirror-health.service.erb'),
    notify  => [Exec['systemctl daemon-reload'], Service["mirror-health-${check_service}"]],
  }

  $service_before = $ensure ? {
    present => [],
    default => [ File[$service_file], ],
  }
  $service_subscribe = $ensure ? {
    present => [ File[$service_file], ],
    default => [],
  }

  $ensure_service = $ensure ? {
    present => running,
    absent  => stopped,
  }
  $ensure_enable = $ensure ? {
    present => true,
    absent  => false,
  }
  service { "mirror-health-${check_service}":
    ensure    => $ensure_service,
    enable    => $ensure_enable,
    require   => Exec['systemctl daemon-reload'],
    before    => $service_before,
    subscribe => $service_subscribe + [ File[ $mirror_health::script ] ],
  }

  $hosts_file = "${mirror_health::confdir}/${check_service}.hosts"
  $tag = "mirror_health::service::${check_service}::hosts"
  concat { $hosts_file:
    ensure         => $ensure,
    ensure_newline => true,
    mode           => '0444',
    notify         => Service["mirror-health-${check_service}"],
  }
  if $this_host_service_name and $ensure == 'present' {
    @@concat::fragment { "mirror_health::service::${check_service}::hosts::${this_host_service_name}":
      tag     => $tag,
      target  => $hosts_file,
      content => $this_host_service_name,
    }
  }
  Concat::Fragment <<| tag == $tag |>>
}
