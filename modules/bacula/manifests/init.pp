# bacula class -- defines all the variables we care about in our bacula deployment
class bacula (
  String  $bacula_operator_email      = 'bacula-reports@admin.debian.org',
  String  $bacula_director_name       = 'debian-dir',
  String  $bacula_storage_name        = 'debian-sd',
  String  $bacula_client_name         = "${::fqdn}-fd",
  String  $bacula_monitor_name        = 'debian-mon',
  String  $bacula_filestor_name       = 'File',
  String  $bacula_filestor_device     = 'FileStorage',
  String  $bacula_pool_name           = 'debian',

  # use IP address for ferm.
  String  $bacula_director_ip_addrs   = '82.195.75.77 2001:41b8:202:deb::311:77',
  String  $bacula_director_address    = 'dinis.debian.org',
  Integer $bacula_director_port       = 9101,
  String  $bacula_storage_address     = 'storace.debian.org',
  Integer $bacula_storage_port        = 9103,
  Integer $bacula_client_port         = 9102,
  String  $bacula_db_address          = 'danzi.debian.org',
  Integer $bacula_db_port             = 5433,

  String  $bacula_backup_path         = '/srv/bacula',

  String  $bacula_director_secret     = hkdf('/etc/puppet/secret', "bacula-dir-${::hostname}"),
  String  $bacula_db_secret           = hkdf('/etc/puppet/secret', "bacula-db-${::hostname}"),
  String  $bacula_storage_secret      = hkdf('/etc/puppet/secret', "bacula-sd-${bacula_storage_name}"),
  String  $bacula_client_secret       = hkdf('/etc/puppet/secret', "bacula-fd-${::fqdn}"),
  String  $bacula_monitor_secret      = hkdf('/etc/puppet/secret', "bacula-monitor-${bacula_director_name}"),

  String  $bacula_ca_path             = '/etc/ssl/debian/certs/ca.crt',
  String  $bacula_ssl_client_cert     = '/etc/ssl/debian/certs/thishost.crt',
  String  $bacula_ssl_client_key      = '/etc/ssl/private/thishost.key',
  String  $bacula_ssl_server_cert     = '/etc/ssl/debian/certs/thishost-server.crt',
  String  $bacula_ssl_server_key      = '/etc/ssl/private/thishost-server.key',

  String  $bacula_dsa_client_list     = '/etc/bacula/dsa-clients',
  String  $tag_bacula_dsa_client_list = 'bacula::dsa::clientlist',
) {
  file { '/usr/local/sbin/bacula-idle-restart':
    mode    => '0555',
    content  => template('bacula/bacula-idle-restart.erb'),
  }
}
