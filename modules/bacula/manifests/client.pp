# our bacula client configuration
#
# this mostly configures the file daemon, but also firewall rules and
# fragments to sent to the other servers.
class bacula::client(
  Enum['present', 'absent'] $ensure = defined(Class["bacula::not_a_client"]) ? { true => 'absent', default => 'present' },
) inherits bacula {
  $package_ensure = $ensure ? { 'present' => 'installed', 'absent' => 'purged' }
  $service_ensure = $ensure ? { 'present' => 'running', 'absent'  => 'stopped' }
  $service_enable = $ensure ? { 'present' => true, 'absent' => false }
  $reverse_ensure = $ensure ? { 'present' => 'absent', 'absent' => 'present' }

  if $ensure == 'present' {
    @@bacula::storage_per_node { $::fqdn: }

    @@bacula::node { $::fqdn:
      bacula_client_port => $bacula::bacula_client_port,
    }

    @@concat::fragment { "bacula-dsa-client-list::$fqdn":
      target  => $bacula::bacula_dsa_client_list ,
      content => @("EOF"),
          ${fqdn}
          | EOF
      tag     => $bacula::tag_bacula_dsa_client_list,
    }
  } elsif $ensure == 'absent' {
    file { '/etc/bacula':
      ensure  => absent,
      purge   => true,
      force   => true,
      recurse => true;
    }
  }

  ensure_packages ( [
   'bacula-fd',
   'bacula-common',
  ], {
    ensure => $package_ensure
  })

  service { 'bacula-fd':
    ensure    => $service_ensure,
    enable    => $service_enable,
    hasstatus => true,
    require   => Package['bacula-fd']
  }

  exec { 'bacula-fd restart-when-idle':
    path        => '/usr/bin:/usr/sbin:/bin:/sbin',
    command     => 'sh -c "setsid /usr/local/sbin/bacula-idle-restart fd &"',
    refreshonly => true,
    subscribe   => [ File[$bacula_ssl_server_cert], File[$bacula_ssl_client_cert] ],
    require     => File['/usr/local/sbin/bacula-idle-restart'],
  }

  file { '/etc/bacula/bacula-fd.conf':
    ensure  => $ensure,
    content => template('bacula/bacula-fd.conf.erb'),
    mode    => '0640',
    owner   => root,
    group   => bacula,
    require => Package['bacula-fd'],
    notify  => Exec['bacula-fd restart-when-idle'],
  }
  file { '/usr/local/sbin/bacula-backup-dirs':
    ensure  => $ensure,
    mode    => '0775',
    source  => 'puppet:///modules/bacula/bacula-backup-dirs',
  }
  file { '/usr/local/sbin/postbaculajob':
    ensure  => $ensure,
    mode    => '0775',
    source  => 'puppet:///modules/bacula/postbaculajob',
  }
  file { '/etc/default/bacula-fd':
    ensure  => $ensure,
    content => template('bacula/default.bacula-fd.erb'),
    mode    => '0400',
    owner   => root,
    group   => root,
    require => Package['bacula-fd'],
    notify  => Service['bacula-fd'],
  }
  if (versioncmp($::lsbmajdistrelease, '9') >= 0 and $systemd) {
    dsa_systemd::override { 'bacula-fd':
      content => @(EOT)
        [Service]
        ExecStart=
        ExecStart=/usr/sbin/bacula-fd -c $CONFIG -f -u bacula -k
        | EOT
    }
  } else {
    dsa_systemd::override { 'bacula-fd':
      ensure  => absent,
    }
  }

  ferm::rule { 'dsa-bacula-fd':
    domain      => '(ip ip6)',
    description => 'Allow bacula access from storage and director',
    rule        => "proto tcp mod state state (NEW) dport (${bacula_client_port}) saddr (${bacula_director_ip_addrs}) ACCEPT",
  }
}
