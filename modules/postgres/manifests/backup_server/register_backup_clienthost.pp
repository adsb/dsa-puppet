#
define postgres::backup_server::register_backup_clienthost (
  $sshpubkey = $::postgres_key,
  $ipaddrlist = join(getfromhash($deprecated::nodeinfo, 'ldap', 'ipHostNumber'), ","),
  $hostname = $::hostname,
) {
  include postgres::backup_server::globals

  if $sshpubkey {
    $addr = assert_type(String[1], $ipaddrlist)
    @@concat::fragment { "postgresql::server::backup-source-clienthost::$name::$fqdn":
      target => $postgres::backup_server::globals::sshkeys_sources ,
      content  => @("EOF"),
          ${hostname} ${addr} ${sshpubkey}
          | EOF
      tag     => $postgres::backup_server::globals::tag_source_sshkey,
    }
  }
}
