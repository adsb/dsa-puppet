#
class postgres::backup_server::globals {
  $make_base_backups = '/usr/local/bin/postgres-make-base-backups'
  $pgpassfile = '/home/debbackup/.pgpass'
  $sshkeys_sources = '/etc/dsa/postgresql-backup/sshkeys-sources'

  $tag_base_backup = "postgresql::server::backup-source-make-base-backup-entry"
  $tag_source_sshkey = "postgresql::server::backup-source-sshkey"
  $tag_source_pgpassline = "postgresql::server::backup-source-pgpassline"
  $tag_dsa_check_backupp = "postgresql::server::backup-dsa-check-backuppg"
}
