#
define postgres::backup_server::register_backup_cluster (
  $hostname = $::hostname,
  $fqdn = $::fqdn,
  $pg_port,
  $pg_role,
  $pg_password,
  $pg_cluster,
  $pg_version,
) {
  include postgres::backup_server::globals

  # foobar.debian.org:5432:*:debian-backup:swordfish
  @@concat::fragment { "postgresql::server::backup-source-pgpassline::$hostname::$pg_port::$pg_role":
    target => $postgres::backup_server::globals::pgpassfile,
    content => @("EOF"),
        ${fqdn}:${pg_port}:*:${pg_role}:${pg_password}
        | EOF
    tag     => $postgres::backup_server::globals::tag_source_pgpassline,
  }
  #
  # vittoria.debian.org  5432  debian-backup    main    9.6
  @@concat::fragment { "postgresql::server::backup-source-make-base-backup-entry::$hostname::$pg_port::$pg_role":
    target => $postgres::backup_server::globals::make_base_backups,
    content => @("EOF"),
        ${fqdn}  ${pg_port}  ${pg_role}  ${pg_cluster}  ${pg_version}
        | EOF
    tag     => $postgres::backup_server::globals::tag_base_backup,
  }

  @@file { "/etc/dsa/postgresql-backup/dsa-check-backuppg.conf.d/${hostname}-${pg_cluster}.conf":
    content  => @("EOF"),
        --- 
        backups:
          ${hostname}:
            ${pg_cluster}:
        | EOF
    tag     => $postgres::backup_server::globals::tag_dsa_check_backupp,
    notify  => Exec['update dsa-check-backuppg-manual.conf']
  }
}
