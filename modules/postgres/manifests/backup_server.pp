#
class postgres::backup_server {
  include postgres::backup_server::globals

  ensure_packages ( [
    'libhash-merge-simple-perl',
    'libyaml-perl',
    'python-yaml',
    'pigz',
    'postgresql-client',
    'postgresql-client-9.6',
  ], {
    ensure => 'installed'
  })

  ####
  # Regularly pull base backups
  #
  concat { $postgres::backup_server::globals::make_base_backups:
    mode => '0555',
  }
  concat::fragment { 'make-base-backups-header':
    target => $postgres::backup_server::globals::make_base_backups,
    content => template('postgres/backup_server/postgres-make-base-backups.erb'),
    order  => '00',
  }
  Concat::Fragment <<| tag == $postgres::backup_server::globals::tag_base_backup |>>
  concat::fragment { 'make-base-backups-tail':
    target => $postgres::backup_server::globals::make_base_backups,
    content  => @(EOTEMPLATE),
        # EOF by make-base-backups-tail fragment
        EOF
        | EOTEMPLATE
    order  => '99',
  }
  file { '/etc/cron.d/puppet-postgres-make-base-backups': ensure => absent; }
  file { '/var/lib/dsa/postgres-make-base-backups':
    ensure => directory,
    owner => 'debbackup',
    mode => '0755',
  }
  concat::fragment { 'puppet-crontab--postgres-make_base_backups':
    target => '/etc/cron.d/puppet-crontab',
    content  => @("EOF")
      */30 * * * * debbackup sleep $(( RANDOM \% 1200 )); chronic ${$postgres::backup_server::globals::make_base_backups}
      | EOF
  }

  ####
  # Maintain authorized_keys file on backup servers for WAL shipping
  #
  # do not let other hosts directly build our authorized_keys file,
  # instead go via a script that somewhat validates intput
  file { '/etc/dsa/postgresql-backup':
    ensure => 'directory',
  }
  file { '/usr/local/bin/postgres-make-backup-sshauthkeys':
    content => template('postgres/backup_server/postgres-make-backup-sshauthkeys.erb'),
    mode   => '0555',
    notify  => Exec['postgres-make-backup-sshauthkeys'],
  }
  file { '/usr/local/bin/postgres-make-one-base-backup':
    source  => 'puppet:///modules/postgres/backup_server/postgres-make-one-base-backup',
    mode   => '0555'
  }
  file { '/etc/dsa/postgresql-backup/sshkeys-manual':
    content => template('postgres/backup_server/sshkeys-manual.erb'),
    notify  => Exec['postgres-make-backup-sshauthkeys'],
  }
  concat { $postgres::backup_server::globals::sshkeys_sources:
    notify  => Exec['postgres-make-backup-sshauthkeys'],
  }
  concat::fragment { 'postgresql-backup/source-sshkeys-header':
    target => $postgres::backup_server::globals::sshkeys_sources ,
    content  => @(EOF),
        # <name> <ip addresses> <key>
        | EOF
    order  => '00',
  }
  Concat::Fragment <<| tag == $postgres::backup_server::globals::tag_source_sshkey |>>
  exec { "postgres-make-backup-sshauthkeys":
    command => "/usr/local/bin/postgres-make-backup-sshauthkeys",
    refreshonly => true,
  }

  ####
  # Maintain /etc/nagios/dsa-check-backuppg.conf
  #
  file { '/etc/dsa/postgresql-backup/dsa-check-backuppg.conf.d':
    ensure => 'directory',
    purge   => true,
    force   => true,
    recurse => true,
    source  => 'puppet:///files/empty/',
    notify => Exec['update dsa-check-backuppg-manual.conf'],
  }
  file { '/etc/dsa/postgresql-backup/dsa-check-backuppg.conf.d/manual.conf':
    content => template('postgres/backup_server/dsa-check-backuppg-manual.conf.erb'),
    notify => Exec['update dsa-check-backuppg-manual.conf']
  }
  File<<| tag == $postgres::backup_server::globals::tag_dsa_check_backupp |>>
  exec { "update dsa-check-backuppg-manual.conf":
    command  => @(EOF),
        perl -MYAML=LoadFile,Dump -MHash::Merge::Simple=merge -E 'say Dump(merge(map{LoadFile($_)}@ARGV))' /etc/dsa/postgresql-backup/dsa-check-backuppg.conf.d/*.conf > /etc/nagios/dsa-check-backuppg.conf
        | EOF
    provider => shell,
    refreshonly => true,
  }

  ####
  # Maintain .pgpass file on backup servers
  # #
  concat { $postgres::backup_server::globals::pgpassfile:
    owner => 'debbackup',
    group => 'debbackup',
    mode  => '0400'
  }
  concat::fragment{ 'pgpass-local':
    target => $postgres::backup_server::globals::pgpassfile,
    source => '/home/debbackup/.pgpass-local',
    order  => '00'
  }
  Concat::Fragment <<| tag == $postgres::backup_server::globals::tag_source_pgpassline |>>
}
