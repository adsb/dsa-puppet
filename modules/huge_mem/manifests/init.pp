class huge_mem {
	# this is included by all hosts,
	# so filtering needs to happen here.

	if $::hostname in [grnet-node01,grnet-node02] {
		base::sysctl { 'puppet-vm_dirty_bytes':
			key   => 'vm.dirty_bytes',
			value => '1073741824',
		}
		base::sysctl { 'puppet-vm_dirty_background_bytes':
			key   => 'vm.dirty_background_bytes',
			value => '268435456',
		}
	}
}
