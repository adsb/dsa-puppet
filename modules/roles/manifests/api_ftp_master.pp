class roles::api_ftp_master {
  include apache2
  ssl::service { 'api.ftp-master.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
}
