class roles::bugs_web {
  class { 'apache2':
    rate_limit => true,
  }

  ssl::service { 'bugs.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  ferm::rule { 'dsa-bugs-abusers':
    prio => '005',
    rule => 'saddr (220.243.135/24 220.243.136/24) DROP',
  }
}
