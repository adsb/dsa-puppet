class roles::rtmaster {
  include apache2
  ssl::service { 'rt.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  # Note that there is also role specific config in exim4.conf
  exim::vdomain { 'rt.debian.org':
    mail_user  => 'rt',
    mail_group => 'rt',
  }
}
