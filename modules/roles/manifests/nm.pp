class roles::nm {
  include apache2
  include roles::sso_rp

  ssl::service { 'nm.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  exim::vdomain { 'nm.debian.org':
    owner => 'nm',
    group => 'nm',
  }
}
