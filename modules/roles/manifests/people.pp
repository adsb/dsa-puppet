class roles::people {
  include apache2
  ssl::service { 'people.debian.org': notify  => Exec['service apache2 reload'], key => true, }
  onion::service { 'people.debian.org': port => 80, target_address => 'people.debian.org', target_port => 80, direct => true }
}
