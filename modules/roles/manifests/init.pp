# = Class: roles
#
class roles {
  if has_role('security_mirror') {
    include roles::security_mirror
  }

  if has_role('postgres_backup_server') {
    include postgres::backup_server
  }

  if has_role('postgresql_server') {
    include postgres::backup_source
  }

  if $::keyring_debian_org_mirror {
    include roles::keyring_debian_org_mirror
  }
}
