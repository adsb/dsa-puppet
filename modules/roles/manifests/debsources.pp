class roles::debsources {
  include apache2
  include apache2::ssl

  apache2::module { 'http2': }

  package { 'libapache2-mod-wsgi': ensure => installed, }

  apache2::site { 'sources.debian.org':
    site   => 'sources.debian.org',
    source => 'puppet:///modules/roles/debsources/sources.debian.org.conf',
  }
  ssl::service { 'sources.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
}
