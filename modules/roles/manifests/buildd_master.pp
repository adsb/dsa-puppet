class roles::buildd_master {
  include apache2
  include roles::sso_rp

  ssl::service { 'buildd.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  ssh::authorized_key_collect { 'buildd-master':
    target_user => 'wb-buildd',
    collect_tag => 'buildd_master',
  }

  exim::vdomain { 'buildd.debian.org':
    owner => 'wbadm',
    group => 'wbadm',
  }
}
