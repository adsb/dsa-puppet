class roles::ftp_master {
  include roles::dakmaster
  include roles::signing

  rsync::site { 'dakmaster':
    source      => 'puppet:///modules/roles/dakmaster/rsyncd.conf',
    # Needs to be at least number of direct mirrors plus some spare
    max_clients => 50,
    sslname     => 'ftp-master.debian.org',
  }

  ssl::service { 'ftp-master.debian.org':
    notify   => Exec['service apache2 reload'],
    key      => true,
    tlsaport => [443, 1873],
  }

  # export ssh allow rules for hosts that we should be able to access
  @@ferm::rule::simple { "dsa-ssh-from-ftp_master-${::fqdn}":
    tag         => 'ssh::server::from::ftp_master',
    description => 'Allow ssh access from ftp_master',
    chain       => 'ssh',
    saddr       => $base::public_addresses,
  }
}
