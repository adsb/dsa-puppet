# db server providing (secondary) snapshot databases
class roles::snapshot_db (
) {
  $now = Timestamp()
  $date = $now.strftime('%F')

  if versioncmp($date, '2020-01-15') <= 0 {
    $ensure = 'present'
  } else {
    $ensure = 'absent'
    notify {'Temporary old pg ignore rule expired, clean up puppet':
      loglevel => warning,
    }
  }
  file { '/etc/nagios/obsolete-packages-ignore.d/puppet-postgres':
    ensure  => $ensure,
    content => @(EOF),
      libperl5.24:amd64
      postgresql-client-9.6
      postgresql-contrib-9.6
      perl-modules-5.24
      postgresql-plperl-9.6
      postgresql-9.6-debversion
      libgdbm3:amd64
      postgresql-9.6
      | EOF
  }

}
