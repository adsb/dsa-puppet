class roles::qamaster {
  include apache2
  include roles::sso_rp

  ssl::service { 'qa.debian.org': notify  => Exec['service apache2 reload'], key => true, }

  exim::vdomain { 'qa.debian.org':
    owner => 'qa',
    group => 'qa',
  }
}
