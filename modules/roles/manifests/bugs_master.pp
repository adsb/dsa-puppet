class roles::bugs_master {
  include roles::bugs_web

  ssl::service { 'bugs-devel.debian.org': notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'bugs-master.debian.org': notify  => Exec['service apache2 reload'], key => true, }

  # Note that there is also role specific config in exim4.conf
  exim::vdomain { 'bugs.debian.org':
    owner => 'debbugs',
    group => 'debbugs',
  }

  # The bugs service accepts bug reports on the submission port
  ferm::rule::simple { 'bugs-submission':
    description => 'Allow submission access from the world',
    port        => 'submission',
  }
}
