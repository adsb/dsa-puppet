# Debian SSO class.
#
# This sets up the web service as well as the LDAP backend for ftmg
class roles::sso {
  include apache2
  include roles::sso_rp

  ssl::service { 'sso.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
  ssl::service { 'ftmg.sso.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  $ftmg_dsa_root_password = hkdf('/etc/puppet/secret', "roles::sso::slapd-ftmg::${::fqdn}")

  ensure_packages ( [
    'slapd',
    ], {
    ensure => 'installed',
  })
  service { 'slapd':
    ensure  => running,
  }
  file { '/etc/ldap/slapd.d':
    ensure => absent,
    force  => true,
    notify => Service['slapd'],
  }
  file { '/etc/ldap/slapd.conf':
    source => 'puppet:///modules/roles/sso/slapd.conf',
    notify => Service['slapd'],
  }
  file { '/etc/ldap/slapd-ftmg.conf':
    content => template('roles/sso/slapd-ftmg.conf.erb'),
    notify  => Service['slapd'],
    group   => 'openldap',
    mode    => '0440',
  }
  file { '/etc/default/slapd':
    source => 'puppet:///modules/roles/sso/default-slapd',
    notify => Service['slapd'],
  }
  file { '/var/lib/ldap-ftmg':
    ensure => directory,
    mode   => '0700',
    owner  => 'openldap',
    group  => 'openldap',
    notify => Service['slapd'],
  }

  file { '/etc/ldap/schema/openssh-ldap.schema':
    source => 'puppet:///modules/roles/sso/openssh-ldap.schema',
    notify => Service['slapd'],
  }
}
