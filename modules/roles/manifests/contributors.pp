class roles::contributors {
  include apache2
  include roles::sso_rp

  ssl::service { 'contributors.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
}
