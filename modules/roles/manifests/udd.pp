class roles::udd {
  class { 'apache2':
    rlimitmem => 512 * 1024 * 1024,
  }

  ssl::service { 'udd.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }
}
