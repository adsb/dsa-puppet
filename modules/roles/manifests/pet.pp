class roles::pet {
  include apache2
  ssl::service { 'pet.debian.net': notify  => Exec['service apache2 reload'], key => true, }
  ssl::service { 'pet-devel.debian.net': notify  => Exec['service apache2 reload'], key => true, }
}
