class autofs {
	case $::hostname {
		pejacevic, piu-slave-bm-a, picconi, coccia, dillon, delfin, quantz, sor, mekeel, tate, respighi: {
			include autofs::bytemark
		}
		lw07,lw08: {
			include autofs::leaseweb
		}
		tye,ullmann,piu-slave-ubc-01,hier,manziarly,lindsay,pinel,ticharich,donizetti: {
			include autofs::ubc
		}
	}
}
