# LVM config for the blades that make up ganeti.bm.debian.org
class profile::lvm::ganeti_bm {
  class { 'lvm':
    global_filter  => '[ "a|^/dev/sda[0-9]*$|", "r/.*/" ]',
  }
}
