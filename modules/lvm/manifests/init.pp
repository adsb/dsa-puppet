# put a lvm.conf file on the host,
#   optionally with the global_filter attribute set,
#   with issue_discards set as configured.
class lvm(
  Optional[String] $global_filter = undef,
  Boolean          $issue_discards = false,
) {
  if (versioncmp($::lsbmajdistrelease, '10') >= 0) {
    $template = 'lvm/lvm.conf-buster.erb'
  } else {
    $template = 'lvm/lvm.conf-stretch.erb'
  }

  file { '/etc/lvm/lvm.conf':
    ensure  => file,
    content => template($template),
    notify  => [ Exec['update-initramfs -u'], Exec['vgscan'] ];
  }

  exec { 'vgscan':
      path        => '/etc/init.d:/usr/bin:/usr/sbin:/bin:/sbin',
      refreshonly => true;
  }
}
