class ferm::per_host {
  if (getfromhash($deprecated::nodeinfo, 'hoster', 'name') == 'aql') {
    include ferm::aql
  }

  case $::hostname {
    czerny,clementi: {
      ferm::rule { 'dsa-upsmon':
        description => 'Allow upsmon access',
        rule        => '&SERVICE_RANGE(tcp, 3493, ( 82.195.75.64/26 192.168.43.0/24 ))'
      }
    }
    kaufmann: {
      ferm::rule { 'dsa-hkp':
        domain      => '(ip ip6)',
        description => 'Allow hkp access',
        rule        => '&SERVICE(tcp, 11371)'
      }
    }
    gombert: {
      ferm::rule { 'dsa-infinoted':
        domain      => '(ip ip6)',
        description => 'Allow infinoted access',
        rule        => '&SERVICE(tcp, 6523)'
      }
    }
    draghi: {
      ferm::rule { 'dsa-finger':
        domain      => '(ip ip6)',
        description => 'Allow finger access',
        rule        => '&SERVICE(tcp, 79)'
      }
      ferm::rule { 'dsa-ldap':
        domain      => '(ip ip6)',
        description => 'Allow ldap access',
        rule        => '&SERVICE(tcp, 389)'
      }
      ferm::rule { 'dsa-ldaps':
        domain      => '(ip ip6)',
        description => 'Allow ldaps access',
        rule        => '&SERVICE(tcp, 636)'
      }
    }
    default: {}
  }

  case $::hostname {
    bm-bl1,bm-bl2: {
      ferm::rule { 'dsa-vrrp':
        rule => 'proto vrrp daddr 224.0.0.18 jump ACCEPT',
      }
      ferm::rule { 'dsa-bind-notrack-in':
        domain      => 'ip',
        description => 'NOTRACK for nameserver traffic',
        table       => 'raw',
        chain       => 'PREROUTING',
        rule        => 'proto (tcp udp) daddr 5.153.231.24 dport 53 jump NOTRACK'
      }

      ferm::rule { 'dsa-bind-notrack-out':
        domain      => 'ip',
        description => 'NOTRACK for nameserver traffic',
        table       => 'raw',
        chain       => 'OUTPUT',
        rule        => 'proto (tcp udp) saddr 5.153.231.24 sport 53 jump NOTRACK'
      }

      ferm::rule { 'dsa-bind-notrack-in6':
        domain      => 'ip6',
        description => 'NOTRACK for nameserver traffic',
        table       => 'raw',
        chain       => 'PREROUTING',
        rule        => 'proto (tcp udp) daddr 2001:41c8:1000:21::21:24 dport 53 jump NOTRACK'
      }

      ferm::rule { 'dsa-bind-notrack-out6':
        domain      => 'ip6',
        description => 'NOTRACK for nameserver traffic',
        table       => 'raw',
        chain       => 'OUTPUT',
        rule        => 'proto (tcp udp) saddr 2001:41c8:1000:21::21:24 sport 53 jump NOTRACK'
      }
    }
    default: {}
  }

  # postgres stuff
  case $::hostname {
    ullmann: {
      ferm::rule { 'dsa-postgres-udd':
        description => 'Allow postgress access',
        domain      => '(ip ip6)',
        # quantz, master, coccia
        rule        => @("EOF")
          &SERVICE_RANGE(tcp, 5452, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'quantz.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'master.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'coccia.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'respighi.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'wuiet.debian.org', 'ipHostNumber'), " ") }
          ))
          | EOF
      }
    }
    fasolo: {
      ferm::rule { 'dsa-postgres':
        description => 'Allow postgress access',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5433, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'bmdb1.debian.org', 'ipHostNumber'), " ") }
            \$HOST_PGBACKUPHOST
          ))
          | EOF
      }
    }
    bmdb1: {
      ferm::rule { 'dsa-postgres-main':
        description => 'Allow postgress access to cluster: main',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5435, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'ticharich.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'petrova.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'ullmann.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'wuiet.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'quantz.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'respighi.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'tate.debian.org', 'ipHostNumber'), " ") }
            \$HOST_PGBACKUPHOST
          ))
          | EOF
      }
      ferm::rule { 'dsa-postgres-dak':
        description => 'Allow postgress access to cluster: dak',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5434, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'coccia.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'quantz.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'nono.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'wuiet.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'respighi.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'usper.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'ullmann.debian.org', 'ipHostNumber'), " ") }
          ))
          | EOF
      }
      ferm::rule { 'dsa-postgres-bacula':
        description => 'Allow postgress access to cluster: bacula',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5437, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'dinis.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'storace.debian.org', 'ipHostNumber'), " ") }
            \$HOST_PGBACKUPHOST
          ))
          | EOF
      }
      ferm::rule { 'dsa-postgres-dedup':
        description => 'Allow postgress access to cluster: dedup',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5439, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'delfin.debian.org', 'ipHostNumber'), " ") }
          ))
          | EOF
      }
      ferm::rule { 'dsa-postgres-debsources':
        description => 'Allow postgress access to cluster: debsources',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5440, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'sor.debian.org', 'ipHostNumber'), " ") }
            \$HOST_PGBACKUPHOST
          ))
          | EOF
      }
    }
    danzi: {
      ferm::rule { 'dsa-postgres-tracker':
        description => 'Allow postgress access to cluster: tracker',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5432, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'ticharich.debian.org', 'ipHostNumber'), " ") }
            \$HOST_PGBACKUPHOST
          ))
          | EOF
      }
      ferm::rule { 'dsa-postgres-main':
        # ubc, wuiet
        description => 'Allow postgress access to cluster: main',
        domain      => '(ip ip6)',
        rule        => '&SERVICE_RANGE(tcp, 5433, ( 209.87.16.0/24 2607:f8f0:614:1::/64 ))'
      }
      ferm::rule { 'dsa-postgres-debconf':
        description => 'Allow postgress access to cluster: debconf',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5434, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'debussy.debian.org', 'ipHostNumber'), " ") }
            \$HOST_PGBACKUPHOST
          ))
          | EOF
      }
      ferm::rule { 'dsa-postgres-wannabuild':
        description => 'Allow postgress access to cluster: wannabuild',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5436, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'respighi.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'wuiet.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'ullmann.debian.org', 'ipHostNumber'), " ") }
            \$HOST_PGBACKUPHOST
          ))
          | EOF
      }
    }
    seger: {
      ferm::rule { 'dsa-postgres-backup':
        description => 'Allow postgress access',
        rule        => '&SERVICE_RANGE(tcp, 5432, ( $HOST_PGBACKUPHOST_V4 ))'
      }
      ferm::rule { 'dsa-postgres-backup6':
        domain      => 'ip6',
        description => 'Allow postgress access',
        rule        => '&SERVICE_RANGE(tcp, 5432, ( $HOST_PGBACKUPHOST_V6 ))'
      }
    }
    sallinen: {
      ferm::rule { 'dsa-postgres':
        description => 'Allow postgress access',
        domain      => '(ip ip6)',
        rule        => @("EOF"/$)
          &SERVICE_RANGE(tcp, 5473, (
            ${ join(getfromhash($deprecated::allnodeinfo, 'lw07.debian.org', 'ipHostNumber'), " ") }
            ${ join(getfromhash($deprecated::allnodeinfo, 'snapshotdb-manda-01.debian.org', 'ipHostNumber'), " ") }
            \$HOST_PGBACKUPHOST
          ))
          | EOF
      }
    }
    lw07: {
      ferm::rule { 'dsa-postgres-snapshot':
        description => 'Allow postgress access',
        rule        => '&SERVICE_RANGE(tcp, 5439, ( 185.17.185.176/28 ))'
      }
      ferm::rule { 'dsa-postgres-snapshot6':
        domain      => 'ip6',
        description => 'Allow postgress access',
        rule        => '&SERVICE_RANGE(tcp, 5439, ( 2001:1af8:4020:b030::/64 ))'
      }
    }
    snapshotdb-manda-01: {
      ferm::rule { 'dsa-postgres-snapshot':
        domain      => '(ip ip6)',
        description => 'Allow postgress access from leaseweb (lw07 and friends)',
        rule        => '&SERVICE_RANGE(tcp, 5442, ( 185.17.185.176/28 2001:1af8:4020:b030::/64 ))'
      }
    }
    default: {}
  }
  # vpn fu
  case $::hostname {
    draghi: {
      ferm::rule { 'dsa-vpn':
        description => 'Allow openvpn access',
        rule        => '&SERVICE(udp, 17257)'
      }
      ferm::rule { 'dsa-routing':
        description => 'forward chain',
        chain       => 'FORWARD',
        rule        => 'policy ACCEPT;
mod state state (ESTABLISHED RELATED) ACCEPT;
interface tun+ ACCEPT;
REJECT reject-with icmp-admin-prohibited
'
      }
      ferm::rule { 'dsa-vpn-mark':
        table => 'mangle',
        chain => 'PREROUTING',
        rule  => 'interface tun+ MARK set-mark 1',
      }
      ferm::rule { 'dsa-vpn-nat':
        table => 'nat',
        chain => 'POSTROUTING',
        rule  => 'outerface !tun+ mod mark mark 1 MASQUERADE',
      }
    }
    ubc-enc2bl01,ubc-enc2bl02,ubc-enc2bl09,ubc-enc2bl10: {
      ferm::rule { 'dsa-ssh-priv':
        description => 'Allow ssh access',
        rule        => '&SERVICE_RANGE(tcp, 22, ( 172.29.40.0/22 172.29.203.0/24 ))',
      }
    }
    ubc-node-arm01,ubc-node-arm02,ubc-node-arm03: {
      ferm::rule { 'dsa-ssh-priv':
        description => 'Allow ssh access',
        rule        => '&SERVICE_RANGE(tcp, 22, ( 172.29.43.240 ))',
      }
    }
    default: {}
  }
  # tftp
  case $::hostname {
    abel: {
      ferm::rule { 'dsa-tftp':
        description => 'Allow tftp access',
        rule        => '&SERVICE_RANGE(udp, 69, ( 172.28.17.0/24 ))'
      }
    }
    master: {
      ferm::rule { 'dsa-tftp':
        description => 'Allow tftp access',
        rule        => '&SERVICE_RANGE(udp, 69, ( 82.195.75.64/26 192.168.43.0/24 ))'
      }
    }
  }
}
