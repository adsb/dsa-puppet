Package {
	require => File['/etc/apt/apt.conf.d/local-recommends']
}

File {
	owner  => root,
	group  => root,
	mode   => '0444',
	ensure => file,
}

Exec {
	path => '/usr/bin:/usr/sbin:/bin:/sbin'
}

Service {
	hasrestart => true,
	hasstatus  => true,
}

node default {
	# we really should rename this one
	include deprecated
	include base

	# this is magic: it will include whatever classes says we should
	# include, based on the value of the "classes" array
	hiera_include('classes')

	include roles::pubsub::client
	class { 'roles::udldap::client':
		ensure => absent
	}

	if getfromhash($deprecated::nodeinfo, 'ganeti') {
		include ganeti2
	}

	if $::kernel == 'Linux' {
		include linux
		include acpi
	}

	if $::apache2 {
		include apache2
	}

	if $::hostname in [buxtehude,milanollo,lw01,lw02,lw03,lw04,lw09,lw10,senfter,gretchaninov,sibelius] {
		include nfs_server
	}

	if $::brokenhosts {
		include hosts
	}

	if $::samhain {
		include samhain
	}

	if $::hostname in [geo3,wieck] {
		include debian_org::radvd
	}

	if $::spamd {
		munin::check { 'spamassassin': }
	}

	if $::hostname == 'pkgmirror-csail' {
		include varnish_pkgmirror
	}
}
